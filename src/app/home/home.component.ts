import { Component, OnInit } from "@angular/core";
import { Gmail } from "../account/provider/gmail";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styles: [],
})
export class HomeComponent implements OnInit {
  constructor(public gmail: Gmail) {}

  ngOnInit() {}
}
