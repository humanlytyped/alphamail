import { Injectable } from "@angular/core";
import { FilterService, ApplyFilterError } from "./filter-manager.interface";
import { MessageFilter } from "src/schemas/message-filter";
import { Account, ProviderName } from "src/app/account/account";
import { MsalService } from "@azure/msal-angular";
import { Client } from "@microsoft/microsoft-graph-client";
import { environment } from "src/environments/environment";
import { fromMessageFilter } from "src/schemas/outlook-filter";
import { name } from "src/app/account/provider/account-provider";

@Injectable()
export class OutlookFilterManager implements FilterService {
  private readonly graphClient: Client;
  [name] = ProviderName.outlook;

  constructor(private readonly msal: MsalService) {
    this.graphClient = Client.init({
      authProvider: done => {
        this.msal
          .acquireTokenSilent(environment.outlook.oauthScopes)
          .then(token => {
            done(null, token);
          })
          .catch(error => done(error, ""));
      },
      defaultVersion: "beta",
    });
  }
  async apply(filter: MessageFilter, account: Account) {
    const messageRule = fromMessageFilter(filter);
    try {
      const response = await this.graphClient
        .api("https://graph.microsoft.com/beta/me/mailFolders/inbox/messagerules")
        .post(messageRule);
      console.log(response);
    } catch (error) {
      throw new ApplyFilterError(error, filter, account);
    }
  }
}
