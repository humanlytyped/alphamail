import { MessageFilter } from "src/schemas/message-filter";
import { Account, ProviderName } from "../../account/account";
import { name } from "src/app/account/provider/account-provider";

export class ApplyFilterError extends Error {
  constructor(public readonly error: Error, public readonly filter: MessageFilter, public readonly account: Account) {
    super(error.message);
  }
}

export class FilterService {}

export interface FilterService {
  [name]: ProviderName;
  /**
   * Set the given filter on the given account.
   *
   * @param {MessageFilter} filter
   * @param {Account} account
   * @memberof FilterService
   * @returns {(void | Promise<void>)}
   * @throws ApplyFilterError
   */
  apply(filter: MessageFilter, account: Account): void | Promise<void>;
}
