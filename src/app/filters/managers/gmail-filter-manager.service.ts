import { Injectable } from "@angular/core";
import { FilterService, ApplyFilterError } from "./filter-manager.interface";
import { name } from "src/app/account/provider/account-provider";
import { ProviderName, Account } from "src/app/account/account";
import { MessageFilter } from "src/schemas/message-filter";
import { Gmail } from "src/app/account/provider/gmail";
import { HttpClient } from "@angular/common/http";
import { fromMessageFilter } from "src/schemas/gmail-filters-parser";
import { environment } from "src/environments/environment";
import { GmailMessageFilter } from "src/schemas/gmail-filters";

@Injectable({
  providedIn: "root",
})
export class GmailFilterManager implements FilterService {
  constructor(private readonly gmailAuth: Gmail, private readonly http: HttpClient) {}
  [name] = ProviderName.gmail;

  async apply(filter: MessageFilter, account: Account) {
    const gmailFilter = fromMessageFilter(filter);
    const token = await this.gmailAuth.getAccessToken();
    const request = this.http.post<GmailMessageFilter>(
      `https://www.googleapis.com/gmail/v1/users/${this.gmailAuth.userId()}/settings/filters`,
      gmailFilter,
      {
        headers: { authorization: `Bearer ${token}` },
        params: { key: environment.gmailAuth.apiKey },
        withCredentials: true,
      },
    );
    request.subscribe({
      next(value) {
        console.log(value);
      },
      error(err) {
        console.error(new ApplyFilterError(err, filter, account));
      },
    });
  }
}
