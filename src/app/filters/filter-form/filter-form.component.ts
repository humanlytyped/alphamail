import { Component, OnInit } from "@angular/core";
import { AbstractControl, FormControl, FormGroup, Validators } from "@angular/forms";
import { compose, cond, defaultTo, identity, map, partialRight, split, trim, filter } from "ramda";
import { Action, MessageFilter, Rule } from "src/schemas/message-filter";
import { isBoolean, isObject, isString } from "util";
import { FilterStoreService } from "../store/filter-store.service";
import { Router } from "@angular/router";

const stringFieldMultiple = compose(
  map(trim),
  filter(notEmpty),
  split(","),
  defaultTo(""),
);

const booleanZeroOrOne = (value: string) => {
  const x = Number.parseInt(value, 10);
  if (Number.isNaN(x)) {
    return null;
  }
  return !!x;
};

const addNonEmptyProps = addPropertiesIf(notEmpty);

@Component({
  selector: "app-filter",
  templateUrl: "./filter-form.component.html",
  styleUrls: ["./filter-form.component.css"],
})
export class FilterFormComponent implements OnInit {
  constructor(private readonly filterStore: FilterStoreService, private readonly router: Router) {}

  form: FormGroup;
  conditions = this.ruleConditions();
  exceptions = this.ruleConditions();
  action = this.ruleActions();

  formSaveError = "";

  ngOnInit() {
    this.form = new FormGroup({
      displayTitle: new FormControl("", Validators.required),
      conditions: this.conditions,
      exceptions: this.exceptions,
      action: this.action,
    });

    this.conditions.setValidators(this.conditionsError.bind(this));
  }

  private conditionsError() {
    const conditionProvided = notEmpty(this.conditions.value);
    const error = conditionProvided ? null : { required: "At least one condition is required" };
    return error;
  }

  private ruleConditions() {
    return new FormGroup({
      from: new FormControl(""),
      to: new FormControl(""),
      subject: new FormControl(""),
      hasAttachment: new FormControl(""),
      size: new FormGroup({
        larger: new FormControl(""),
        smaller: new FormControl(""),
      }),
      cc: new FormControl(""),
      ccOrTo: new FormControl(""),
      contains: new FormGroup({
        body: new FormControl(),
        bodyOrSubject: new FormControl(),
        subject: new FormControl(),
        to: new FormControl(),
      }),
    });
  }

  private ruleActions() {
    return new FormGroup(
      {
        addLabels: new FormControl(),
        copyToFolder: new FormControl(),
        delete: new FormControl(null),
        forwardTo: new FormControl("", Validators.email),
        markAsRead: new FormControl(null),
        moveToFolder: new FormControl(),
        removeLabels: new FormControl(),
        stopProcessingRules: new FormControl(null),
      },
      control => {
        const actionNotEmpty = notEmpty(control.value);
        const error = actionNotEmpty ? null : { required: "At least one action is required" };
        return error;
      },
    );
  }

  private filterFromForm(): MessageFilter {
    return {
      displayTitle: this.form.get("displayTitle").value,
      conditions: this.ruleFromForm(this.form.get("conditions")),
      exceptions: this.ruleFromForm(this.form.get("exceptions")),
      action: this.actionFromForm(this.form.get("action")),
    };
  }

  private ruleFromForm(conditions: AbstractControl): Rule {
    const rule: Rule = {};
    const extract = extractorFromControl(conditions);
    const [from, to, cc, ccOrTo] = map(partialRight(extract, [stringFieldMultiple]), ["from", "to", "cc", "ccOrTo"]);
    const hasAttachment = extract("hasAttachment", booleanZeroOrOne);
    const subject = extract("subject");
    const [larger, smaller] = map(partialRight(extract, [Number.parseInt]), ["size.larger", "size.smaller"]);
    const sizeComp = addNonEmptyProps({}, new Map([["larger", larger], ["smaller", smaller]]));
    const [bodyContains, bodyOrSubjectContains, subjectContains, toContains] = map(
      partialRight(extract, [stringFieldMultiple]),
      ["contains.body", "contains.bodyOrSubject", "contains.subject", "contains.to"],
    );
    const contains = addNonEmptyProps(
      {},
      new Map([
        ["body", bodyContains],
        ["bodyOrSubject", bodyOrSubjectContains],
        ["subject", subjectContains],
        ["to", toContains],
      ]),
    );
    addNonEmptyProps(
      rule,
      new Map([
        ["from", from],
        ["to", to],
        ["cc", cc],
        ["ccOrTo", ccOrTo],
        ["subject", subject],
        ["hasAttachment", hasAttachment],
        ["size", sizeComp],
        ["contains", contains],
      ]),
    );
    return rule;
  }

  private actionFromForm(formActions: AbstractControl): Action {
    const action: Action = {};
    const extract = extractorFromControl(formActions);
    const [copyToFolder, moveToFolder] = map(extract, ["copyToFolder", "moveToFolder"]);
    const [addLabels, forwardTo, removeLabels] = map(partialRight(extract, [stringFieldMultiple]), [
      "addLabels",
      "forwardTo",
      "removeLabels",
    ]);
    const [deleteMessage, markAsRead, stopProcessingRules] = map(extract, [
      "delete",
      "markAsRead",
      "stopProcessingRules",
    ]);
    addNonEmptyProps(
      action,
      new Map([
        ["copyToFolder", copyToFolder],
        ["moveToFolder", moveToFolder],
        ["addLabels", addLabels],
        ["forwardTo", forwardTo],
        ["removeLabels", removeLabels],
        ["delete", deleteMessage],
        ["markAsRead", markAsRead],
        ["stopProcessingRules", stopProcessingRules],
      ]),
    );
    return action;
  }

  async submit() {
    const filterToSave = this.filterFromForm();
    try {
      await this.filterStore.save(filterToSave);
      this.formSaveError = "";
      this.router.navigate(["/"]);
    } catch (error) {
      this.formSaveError = error.message;
    }
  }
}

function extractorFromControl(control: AbstractControl) {
  const extract = (path: string, transform?: (v: any) => any) => {
    const value = control.get(path).value;
    return typeof transform === "undefined" ? value : transform(value);
  };
  return extract;
}

function notEmpty(obj: any): boolean {
  return cond([
    [Array.isArray, [].some.bind(obj, notEmpty)],
    [isObject, () => Object.values(obj).reduce((acc, val) => acc || notEmpty(val), false)],
    [isString, () => obj !== ""],
    [isBoolean, () => true],
    [identity, identity],
  ])(obj);
}

function addPropertiesIf(predicate: (x: any) => boolean) {
  return (obj: { [x: string]: any }, props: Map<string, unknown>) => {
    for (const [prop, value] of props.entries()) {
      if (predicate(value)) {
        obj[prop] = value;
      }
    }
    return obj;
  };
}
