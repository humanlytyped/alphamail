import { Component, OnInit } from "@angular/core";
import { FilterStoreService } from "./store/filter-store.service";
import { MessageFilter } from "src/schemas/message-filter";
import { Account } from "../account/account";
import { FilterManager } from "./managers/filter-manager";
import { AccountService } from "../account/account.service";

type filterAction = "apply filter" | "cancel" | "delete filter";

@Component({
  selector: "app-filters",
  templateUrl: "./filters.component.html",
  styleUrls: ["./filters.component.css"],
})
export class FiltersComponent implements OnInit {
  constructor(
    private readonly filterStore: FilterStoreService,
    private readonly filterManager: FilterManager,
    private readonly accountSvc: AccountService,
  ) {
    this.filters = this.filterStore.retrieveAll();
    this.selectedFilter = (this.filters && this.filters[0]) || null;
  }
  filters: MessageFilter[];
  confirmActionMessage: { title: string; details: string; options: filterAction[] } = {
    title: "",
    details: "",
    options: [],
  };
  modal = { visible: false };
  selectedFilter: MessageFilter;
  private selectedAccount: Account = null;

  handleApply(filter: MessageFilter) {
    this.selectedFilter = filter;
    this.confirmActionMessage.title = "Apply filter";
    this.confirmActionMessage.details = this.selectedAccount
      ? `Apply the selected filter to email account '${this.selectedAccount.email}'.

    Do you want to proceed?`
      : "No email selected";
    this.confirmActionMessage.options = ["cancel", "apply filter"];
    this.modal.visible = true;
  }

  async handleSelection(choice: { selectedOption: filterAction }) {
    const selectedAccount = this.selectedAccount;
    this.modal.visible = false;
    try {
      switch (choice.selectedOption) {
        case "cancel":
          return;
        case "apply filter":
          await this.filterManager.apply(this.selectedFilter, selectedAccount);
          break;
        case "delete filter":
          this.handleDeleteFilter();
          break;
        default:
          break;
      }
    } catch (error) {
      console.error(error);
    }
  }

  deleteFilter(filter: MessageFilter) {
    this.selectedFilter = filter;
    this.confirmActionMessage.title = "Delete filter";
    this.confirmActionMessage.details = `Delete the selected filter. This action is not reversible.

    Do you want to proceed?`;
    this.confirmActionMessage.options = ["cancel", "delete filter"];
    this.modal.visible = true;
  }

  handleDeleteFilter() {
    this.filters = this.filters.filter(f => f.key !== this.selectedFilter.key);
    this.filterStore.remove(this.selectedFilter.key);
  }

  ngOnInit() {
    this.accountSvc.listenForAccountEvents({
      onAccountSelected: account => {
        this.selectedAccount = account;
      },
    });
  }
}
