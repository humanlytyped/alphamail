import { TestBed } from "@angular/core/testing";

import { FilterStoreService, FilterRecordConflict } from "./filter-store.service";
import { MessageFilter } from "src/schemas/message-filter";
import { clone } from "ramda";
import slug from "slug";

function filterWithTitle(title: string): MessageFilter {
  return {
    action: {
      delete: true,
    },
    conditions: {
      hasAttachment: true,
    },
    displayTitle: title,
    key: "",
  };
}

describe("FilterStoreService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({});
    localStorage.clear();
  });

  afterAll(doneFn => {
    localStorage.clear();
    doneFn();
  });

  it("should be created", () => {
    const service: FilterStoreService = TestBed.get(FilterStoreService);
    expect(service).toBeTruthy();
  });

  const filter: MessageFilter = filterWithTitle("Delete messages with attachments");

  it("Stores a given filter with a unique key", async () => {
    const service: FilterStoreService = TestBed.get(FilterStoreService);
    const storedFilter = await service.save(filter);
    return expect(storedFilter.key).not.toBeFalsy();
  });

  it("Stores a date with the filter saved", async () => {
    const service: FilterStoreService = TestBed.get(FilterStoreService);
    const storedFilter = await service.save(filter);
    return expect(storedFilter.dateCreated).toBeTruthy();
  });

  it("Throws a conflict error if a new filter's key already exists in the store", async () => {
    const service: FilterStoreService = TestBed.get(FilterStoreService);
    const storedFilter = await service.save(filter);
    const expectedError = new FilterRecordConflict(storedFilter);
    return service
      .save(storedFilter)
      .then(result => fail({ expected: expectedError, actual: result }))
      .catch(error => expect(error).toEqual(expectedError));
  });

  it("Disallows storing multiple filters with the same display title", async () => {
    const service: FilterStoreService = TestBed.get(FilterStoreService);
    const anotherFilterWithSameTitle = clone(filter);
    anotherFilterWithSameTitle.key = slug(filter.displayTitle, { lower: true });
    await service.save(filter);
    const expectedError = new FilterRecordConflict(anotherFilterWithSameTitle);
    return service
      .save(anotherFilterWithSameTitle)
      .then(result => fail({ expected: expectedError, actual: result }))
      .catch(error => expect(error).toEqual(expectedError));
  });

  it("Can retrieve a previously stored filter by its key", async () => {
    const service: FilterStoreService = TestBed.get(FilterStoreService);
    const storedFilter = await service.save(filter);
    const retrievedFilter: MessageFilter = await service.retrieve(storedFilter.key);
    return expect(retrievedFilter).toEqual(storedFilter);
  });

  it("Can retrieve all previously stored filters", async () => {
    const service: FilterStoreService = TestBed.get(FilterStoreService);
    const filtersToStore = ["first", "second", "third"].map(filterWithTitle);
    for (const f of filtersToStore) {
      await service.save(f);
    }
    const storedFilters = await service.retrieveAll();
    return expect(storedFilters.length).toEqual(filtersToStore.length);
  });

  it("Can delete an existing filter from the store", async () => {
    const service: FilterStoreService = TestBed.get(FilterStoreService);
    const storedFilter = await service.save(filter);
    await service.remove(storedFilter.key);
    return expect(await service.retrieve(storedFilter.key)).toBeFalsy();
  });
});
