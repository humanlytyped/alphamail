export enum ProviderName {
  gmail = "gmail",
  outlook = "outlook",
}

export interface Account {
  email: string;
  name: string;
  avatarUrl: string;
  provider: ProviderName;
  createdOn: Date;
}
