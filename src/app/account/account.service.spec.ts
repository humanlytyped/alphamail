import { TestBed } from "@angular/core/testing";

import { AccountService } from "./account.service";
import { HttpClientModule } from "@angular/common/http";
import { Gmail } from "./provider/gmail";
import { Outlook } from "./provider/outlook";
import { AuthModule, OidcSecurityService } from "angular-auth-oidc-client";
import { RouterTestingModule } from "@angular/router/testing";
import { MsalModule } from "@azure/msal-angular";

describe("AccountService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule, AuthModule.forRoot(), MsalModule.forRoot({ clientID: "" }), RouterTestingModule],
      providers: [Gmail, Outlook, OidcSecurityService],
    }),
  );

  it("should be created", () => {
    const service: AccountService = TestBed.get(AccountService);
    expect(service).toBeTruthy();
  });
});
