import { Injectable } from "@angular/core";
import { AccountProvider, name } from "./account-provider";
import { Account, ProviderName } from "../account";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";

// tslint:disable-next-line: no-string-literal
const gapi = window["gapi"];

function gapiClient(): Promise<any> {
  let authInstance = null;
  return new Promise((resolve, reject) => {
    if (authInstance != null) {
      return resolve(authInstance);
    }
    gapi.load("client:auth2", {
      callback() {
        gapi.client
          .init({
            apiKey: environment.gmailAuth.apiKey,
            clientId: environment.gmailAuth.clientId,
            discoveryDocs: [environment.gmailAuth.discoveryUrl],
            scope: environment.gmailAuth.scope.join(" "),
          })
          .then(() => {
            // tslint:disable-next-line: no-string-literal
            authInstance = gapi["auth2"].getAuthInstance();
            resolve(authInstance);
          })
          .catch(reject);
      },
    });
  });
}

@Injectable()
export class Gmail implements AccountProvider {
  [name] = ProviderName.gmail;
  private currentUserId: string;

  constructor(private router: Router) {}

  async logout(): Promise<void> {
    const auth = await gapiClient();
    await auth.signOut();
    this.router.navigateByUrl("");
  }

  async login(): Promise<Account> {
    const auth = await gapiClient();
    return auth
      .signIn({ prompt: "select_account", ux_mode: "redirect", redirect_uri: environment.gmailAuth.redirectUri })
      .then(() => this.getUser());
  }

  async getUser(): Promise<Account> {
    try {
      const auth = await gapiClient();
      if (!auth.isSignedIn.get()) {
        throw new Error("no account found: " + ProviderName.gmail);
      } else {
        const user = auth.currentUser.get().getBasicProfile();
        this.currentUserId = user.getId();
        return {
          avatarUrl: user.getImageUrl(),
          createdOn: new Date(),
          email: user.getEmail(),
          name: user.getName(),
          provider: ProviderName.gmail,
        };
      }
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  userId() {
    return this.currentUserId;
  }

  async getAccessToken(): Promise<string> {
    const auth = await gapiClient();
    const { access_token } = auth.currentUser.get().getAuthResponse(true);
    return access_token;
  }
}
