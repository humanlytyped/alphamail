import { Injectable } from "@angular/core";
import { AccountProvider, name } from "./account-provider";
import { MsalService } from "@azure/msal-angular";
import { ProviderName } from "../account";
import { environment } from "src/environments/environment";

const oauthScopes = environment.outlook.oauthScopes;

@Injectable()
export class Outlook implements AccountProvider {
  [name] = ProviderName.outlook;
  constructor(private msal: MsalService) {}

  async logout() {
    return this.msal.logout();
  }
  async login() {
    await this.msal.loginRedirect(oauthScopes);
    return this.getUser();
  }

  async getUser() {
    const user = this.msal.getUser();
    if (!user) {
      throw new Error("no user found");
    }
    return {
      avatarUrl: `https://ui-avatars.com/api/?name=${user.name.split(" ").join("+")}`,
      createdOn: new Date(),
      email: user.displayableId,
      name: user.name,
      provider: ProviderName.outlook,
    };
  }

  getAccessToken() {
    return this.msal.acquireTokenSilent(oauthScopes);
  }
}
