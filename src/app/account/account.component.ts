import { Component, OnInit } from "@angular/core";
import { AccountService } from "./account.service";
import { Account, ProviderName } from "./account";

@Component({
  selector: "app-account",
  templateUrl: "./account.component.html",
  styleUrls: ["./account.component.css"],
})
export class AccountComponent implements OnInit {
  accounts: Account[] = [];
  modalState = { visible: false };
  confirmActionMessage = {
    title: "",
    details: "",
    options: [],
  };
  ProviderName = ProviderName;
  selectedAccount: Account = null;
  private pendingAccountDeletion: Account;

  constructor(private accountSvc: AccountService) {}

  ngOnInit() {
    this.accountSvc.listenForAccountEvents({
      onLogin: account => {
        if (!this.accounts.some(a => a.email === account.email)) {
          this.accounts.push(account);
        }
        if (this.selectedAccount == null) {
          this.updateSelectedAccount(this.accounts[0]);
        }
      },
    });
  }

  async addAccount(provider: ProviderName) {
    const account = await this.accountSvc.addAccount(provider);
    this.removeAccountFromUI(account);
    this.accounts.push(account);
  }

  toggleModal() {
    this.modalState.visible = !this.modalState.visible;
  }

  confirmDeleteAccount(account: Account) {
    this.confirmActionMessage.title = "Remove Account";
    this.confirmActionMessage.details = `Log out of the email account '${account.email}'.

    Do you want to proceed?`;
    this.confirmActionMessage.options = ["cancel", "proceed"];
    this.pendingAccountDeletion = account;
    this.toggleModal();
  }

  handleSelection(choice: { selectedOption: "cancel" | "proceed" }) {
    this.toggleModal();
    if (choice.selectedOption === "cancel") {
      this.pendingAccountDeletion = null;
      return;
    }
    if (!this.pendingAccountDeletion) {
      return;
    }
    this.deleteAccount(this.pendingAccountDeletion);
  }

  updateSelectedAccount(account: Account) {
    this.selectedAccount = account;
    this.accountSvc.selectAccount(account);
  }

  async deleteAccount(account: Account) {
    console.log(`Deleting account...\n${account.email}`);
    await this.accountSvc.deleteAccount(account);
    this.removeAccountFromUI(account);
  }

  private removeAccountFromUI(account: Account) {
    this.accounts = this.accounts.filter(displayedAccount => account.email !== displayedAccount.email);
    this.updateSelectedAccount(this.accounts[0]);
  }
}
