import { Injectable, EventEmitter, OnDestroy } from "@angular/core";
import { Account, ProviderName } from "./account";
import { AccountProvider } from "./provider/account-provider";
import { Gmail } from "./provider/gmail";
import { Outlook } from "./provider/outlook";
import { Subscription } from "rxjs";

export interface AccountEventListener {
  onLogin?(account: Account): void;
  onRemoval?(account: Account): void;
  onAccountSelected?(account: Account): void;
}

@Injectable({
  providedIn: "root",
})
export class AccountService implements OnDestroy {
  private providers: Map<ProviderName, AccountProvider> = new Map();
  private loginEvents = new EventEmitter<Account>();
  private accountRemovals = new EventEmitter<Account>();
  private accountSelection = new EventEmitter<Account>(true);
  private eventSubscriptions: Subscription[] = [];

  private loggedInAccounts: Account[] = [];
  private haveLoadedAccounts = false;

  private lastAccountSelection: Account = null;

  ngOnDestroy(): void {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
    this.loggedInAccounts = null;
  }
  constructor(gmail: Gmail, outlook: Outlook) {
    this.providers = this.providers.set(ProviderName.gmail, gmail).set(ProviderName.outlook, outlook);
  }

  listenForAccountEvents(listener: AccountEventListener) {
    const subscribeTo = (emitter: EventEmitter<Account>, observer: (a: Account) => void) => {
      const subscription = emitter.subscribe(observer);
      this.eventSubscriptions.push(subscription);
    };
    if (typeof listener.onLogin === "function") {
      if (!this.haveLoadedAccounts) {
        this.loadAccounts();
      } else {
        for (const account of this.loggedInAccounts) {
          listener.onLogin(account);
        }
      }
      subscribeTo(this.loginEvents, listener.onLogin);
    }
    if (typeof listener.onRemoval === "function") {
      subscribeTo(this.accountRemovals, listener.onRemoval);
    }
    if (typeof listener.onAccountSelected === "function") {
      if (this.lastAccountSelection != null) {
        listener.onAccountSelected(this.lastAccountSelection);
      }
      subscribeTo(this.accountSelection, listener.onAccountSelected);
    }
  }

  selectAccount(account) {
    this.accountSelection.emit(account);
  }

  loadAccounts() {
    for (const provider of this.providers.values()) {
      provider
        .getUser()
        .then(account => {
          this.loggedInAccounts.push(account);
          this.loginEvents.emit(account);
        })
        .catch(console.error.bind(console))
        .finally(() => {
          this.haveLoadedAccounts = true;
        });
    }
  }

  async addAccount(provider: ProviderName) {
    const p = this.providers.get(provider);
    const account = await p.login();
    this.loginEvents.emit(account);
    return account;
  }

  async deleteAccount(account: Account) {
    const p = this.providers.get(account.provider);
    this.accountRemovals.emit(account);
    p.logout();
  }
}
