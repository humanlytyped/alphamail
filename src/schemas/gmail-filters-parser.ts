import {
  MessageFilter,
  Action as MessageFilterAction,
  Rule as MessageFilterCriteria,
  Size,
  Contains,
} from "./message-filter";
import { GmailMessageFilter, Action as GmailFilterActions, Criteria as GmailFilterCriteria } from "./gmail-filters";
import { map, pipe } from "ramda";

export function fromMessageFilter(filter: MessageFilter): GmailMessageFilter {
  const gmailFilter: GmailMessageFilter = {
    action: gmailFilterActions(filter.action),
    criteria: gmailFilterCriteria(filter.conditions, filter.exceptions),
  };
  return gmailFilter;
}

const quote = (t: string) => `"${t}"`;
const quoteStrings = map(quote);

const prefix = (p: string) => (t: string) => p + t;
const prefixWith = (p: string) => map(prefix(p));

const quoteThenPrefix = (p: string) =>
  pipe(
    quote,
    prefix(p),
  );
const quoteThenPrefixWith = (p: string) => map(quoteThenPrefix(p));

function gmailFilterCriteria(criteria: MessageFilterCriteria, exceptions?: MessageFilterCriteria): GmailFilterCriteria {
  const gmailCriteria: GmailFilterCriteria = { query: "" };
  const gmailQuery = [];
  if (criteria.from) {
    if (criteria.from.length === 1) {
      gmailCriteria.from = criteria.from[0];
    } else {
      gmailQuery.push(`{${prefixWith("from:")(criteria.from).join(" ")}}`);
    }
  }

  if (criteria.cc) {
    gmailQuery.push(`{${prefixWith("cc:")(criteria.cc).join(" ")}}`);
  }

  if (criteria.ccOrTo) {
    const ccs = prefixWith("cc:")(criteria.ccOrTo);
    const tos = prefixWith("to:")(criteria.ccOrTo);
    gmailQuery.push(`{${ccs.concat(tos).join(" ")}}`);
  }

  if (criteria.hasAttachment != null) {
    gmailCriteria.hasAttachment = criteria.hasAttachment;
  }

  if (criteria.size) {
    const size: GmailSizeComparison = gmailSizeComparison(criteria.size);
    if (size.type === "query") {
      gmailQuery.push(size.query);
    } else {
      gmailCriteria.size = size.size;
      gmailCriteria.sizeComparison = size.sizeComparison;
    }
  }

  if (criteria.subject) {
    if (criteria.subject.length === 1) {
      gmailCriteria.subject = criteria.subject[0];
    } else {
      gmailQuery.push(quoteThenPrefixWith("subject:")(criteria.subject).join(" OR "));
    }
  }

  if (criteria.to) {
    if (criteria.to.length === 1) {
      gmailCriteria.to = criteria.to[0];
    } else {
      gmailQuery.push(prefixWith("to:")(criteria.to).join(" OR "));
    }
  }

  if (criteria.contains) {
    gmailQuery.push(containsQuery(criteria.contains));
  }

  if (exceptions) {
    gmailCriteria.negatedQuery = gmailQueryFromRule(exceptions);
  }
  gmailCriteria.query = gmailQuery.join(" ");
  return gmailCriteria;
}

function containsQuery(contains: Contains): string {
  const queryParts = [];
  if (contains.body) {
    const phrases = quoteStrings(contains.body);
    queryParts.push(`{${phrases.join(" ")}}`);
  }
  if (contains.bodyOrSubject) {
    const phrases = `{${quoteStrings(contains.bodyOrSubject).join(" ")}}`;
    const subjectQuery = prefix("subject:")(phrases);
    queryParts.push(`${phrases} OR ${subjectQuery}`);
  }
  if (contains.subject) {
    const phrases = quoteStrings(contains.subject).join(" ");
    const subjectQuery = prefix("subject:")(`{${phrases}}`);
    queryParts.push(subjectQuery);
  }
  if (contains.to) {
    const phrases = quoteStrings(contains.to).join(" ");
    const toQuery = prefix("to:")(`{${phrases}}`);
    queryParts.push(toQuery);
  }
  return queryParts.join(" ");
}

type GmailSizeComparison =
  | { size: number; sizeComparison: string; type: "property" }
  | { query: string; type: "query" };

const kbToMb = (kb: number) => kb * (1 << 10);

function gmailSizeComparison({ larger, range, smaller }: Size): GmailSizeComparison {
  function comparisonQuery(gt: number, lt: number) {
    return `larger:${gt} smaller:${lt}`;
  }
  let sizeComparison: GmailSizeComparison = { query: "", type: "query" };
  if (range) {
    const [min, max] = range.map(kbToMb);
    sizeComparison = {
      query: comparisonQuery(min - 1, max + 1),
      type: "query",
    };
  } else if (smaller != null && larger != null) {
    sizeComparison = {
      query: comparisonQuery(kbToMb(larger) - 1, kbToMb(smaller) + 1),
      type: "query",
    };
  } else if (smaller != null) {
    sizeComparison = {
      size: kbToMb(smaller),
      sizeComparison: "smaller",
      type: "property",
    };
  } else if (larger != null) {
    sizeComparison = {
      size: kbToMb(larger),
      sizeComparison: "larger",
      type: "property",
    };
  }
  return sizeComparison;
}

function gmailFilterActions(actions: MessageFilterAction): GmailFilterActions {
  const addLabelIds = new Set<string>();
  const removeLabelIds = new Set<string>();
  const gmailActions: GmailFilterActions = {
    addLabelIds: [],
    removeLabelIds: [],
  };
  if (actions.delete) {
    addLabelIds.add("TRASH");
  }
  if (actions.addLabels) {
    actions.addLabels.forEach(addLabelIds.add.bind(addLabelIds));
  }
  if (actions.copyToFolder) {
    addLabelIds.add(actions.copyToFolder);
  }
  if (actions.forwardTo) {
    const [forwardTo] = actions.forwardTo;
    gmailActions.forward = forwardTo;
  }
  if (actions.markAsRead) {
    removeLabelIds.add("UNREAD");
  }
  if (actions.moveToFolder) {
    addLabelIds.add(actions.moveToFolder);
    removeLabelIds.add("INBOX");
  }
  if (actions.removeLabels) {
    actions.removeLabels.forEach(removeLabelIds.add.bind(removeLabelIds));
  }
  gmailActions.addLabelIds = [...addLabelIds.values()];
  gmailActions.removeLabelIds = [...removeLabelIds.values()];
  return gmailActions;
}

function gmailQueryFromRule(rule: MessageFilterCriteria) {
  const query = [];
  if (rule.from) {
    const phrases = prefixWith("from:")(rule.from).join(" ");
    query.push(`{${phrases}}`);
  }
  if (rule.hasAttachment != null) {
    if (rule.hasAttachment) {
      query.push("has:attachment");
    } else {
      query.push("-has:attachment");
    }
  }
  if (rule.size != null) {
    const comparisonQuery = (floor: number, ceiling: number) => `larger:${floor - 1} smaller:${ceiling + 1}`;
    if (rule.size.range) {
      const [min, max] = rule.size.range.map(kbToMb);
      query.push(comparisonQuery(min, max));
    } else if (rule.size.larger && rule.size.smaller) {
      const [larger, smaller] = [rule.size.larger, rule.size.smaller].map(kbToMb);
      query.push(comparisonQuery(larger, smaller));
    } else if (rule.size.larger) {
      query.push(`larger:${kbToMb(rule.size.larger)}`);
    } else if (rule.size.smaller) {
      query.push(`smaller:${kbToMb(rule.size.smaller)}`);
    }
  }
  if (rule.subject) {
    query.push(`{${quoteThenPrefixWith("subject:")(rule.subject).join(" ")}}`);
  }
  if (rule.to) {
    query.push(`{${quoteThenPrefixWith("to:")(rule.to).join(" ")}}`);
  }
  if (rule.cc) {
    query.push(`{${quoteThenPrefixWith("cc:")(rule.cc).join(" ")}}`);
  }
  if (rule.ccOrTo) {
    const ccQuery = quoteThenPrefixWith("cc:")(rule.ccOrTo);
    const toQuery = quoteThenPrefixWith("to:")(rule.ccOrTo);
    query.push(`{${ccQuery.concat(toQuery).join(" ")}}`);
  }
  if (rule.contains) {
    query.push(containsQuery(rule.contains));
  }
  return query.join(" ");
}
