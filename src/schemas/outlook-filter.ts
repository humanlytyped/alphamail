import {} from "@microsoft/microsoft-graph-client";
import {
  MessageRule,
  MessageRuleActions,
  MessageRulePredicates,
  Recipient,
  SizeRange,
} from "@microsoft/microsoft-graph-types-beta";
import { MessageFilter, Action, Rule, Size } from "./message-filter";

export function fromMessageFilter(filter: MessageFilter): MessageRule {
  const rule: MessageRule = {
    actions: filterAction(filter.action),
    conditions: filterMessageRulePredicate(filter.conditions),
    displayName: filter.displayTitle,
    sequence: 1,
    isEnabled: true,
  };
  if (filter.exceptions) {
    rule.exceptions = filterMessageRulePredicate(filter.exceptions);
  }
  return rule;
}

const outlookRecipient = (address: string): Recipient => ({
  emailAddress: { address },
});

function filterAction(appAction: Action): MessageRuleActions {
  const action: MessageRuleActions = {};
  if (appAction.copyToFolder) {
    action.copyToFolder = appAction.copyToFolder;
  }
  if (typeof appAction.delete !== "undefined") {
    action.delete = appAction.delete;
  }
  if (appAction.forwardTo) {
    action.forwardTo = appAction.forwardTo.map(outlookRecipient);
  }
  if (typeof appAction.markAsRead !== "undefined") {
    action.markAsRead = appAction.markAsRead;
  }
  if (appAction.moveToFolder) {
    action.moveToFolder = appAction.moveToFolder;
  }
  return action;
}

function filterMessageRulePredicate(rule: Rule): MessageRulePredicates {
  const predicate: MessageRulePredicates = {};
  const recipientContains = new Set<string>();
  if (rule.cc) {
    rule.cc.forEach(recipientContains.add.bind(recipientContains));
  }
  if (rule.ccOrTo) {
    rule.ccOrTo.forEach(recipientContains.add.bind(recipientContains));
  }
  if (rule.to) {
    predicate.sentToAddresses = rule.to.map(outlookRecipient);
  }
  if (rule.from) {
    predicate.fromAddresses = rule.from.map(outlookRecipient);
  }
  if (typeof rule.hasAttachment !== "undefined") {
    predicate.hasAttachments = rule.hasAttachment;
  }
  if (rule.subject) {
    predicate.subjectContains = rule.subject;
  }
  if (rule.size) {
    predicate.withinSizeRange = messageSizeRange(rule.size);
  }
  if (rule.contains) {
    if (rule.contains.body) {
      predicate.bodyContains = rule.contains.body;
    }
    if (rule.contains.bodyOrSubject) {
      predicate.bodyOrSubjectContains = rule.contains.bodyOrSubject;
    }
    if (rule.contains.subject) {
      predicate.subjectContains = rule.contains.subject;
    }
    if (rule.contains.to) {
      rule.contains.to.forEach(recipientContains.add.bind(recipientContains));
    }
  }
  if (recipientContains.size) {
    predicate.recipientContains = [...recipientContains];
  }
  return predicate;
}

function messageSizeRange(size: Size): SizeRange {
  const range: SizeRange = {};
  if (size.range) {
    const [min, max] = [size.range[0], size.range[1]];
    range.maximumSize = max;
    range.minimumSize = min;
  } else if ([size.larger, size.smaller].every(Number.isSafeInteger)) {
    range.minimumSize = size.larger;
    range.maximumSize = size.smaller;
  } else if (size.larger != null) {
    range.minimumSize = size.larger;
    range.maximumSize = 2 ** 16 - 1;
  } else if (size.smaller) {
    range.minimumSize = 0;
    range.maximumSize = size.smaller;
  }
  return range;
}
