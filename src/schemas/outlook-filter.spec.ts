import { fromMessageFilter } from "./outlook-filter";

describe("Creating a MessageRule from app message filter", () => {
  describe("Given some app message filter action", () => {
    it("Ignores the addLabels action (it does not apply to Outlook)", () => {
      const outlookFilter = fromMessageFilter({
        action: { addLabels: ["work", "personal"] },
        conditions: {},
        displayTitle: "",
      });
      expect(outlookFilter.actions).toEqual({});
    });

    it("Adds the copyToFolder action if present", () => {
      const outlookFilter = fromMessageFilter({
        action: { copyToFolder: "test" },
        conditions: {},
        displayTitle: "",
      });
      expect(outlookFilter.actions.copyToFolder).toEqual("test");
    });

    it("Adds the delete action if present", () => {
      const outlookFilterDelete = fromMessageFilter({
        action: { delete: true },
        conditions: {},
        displayTitle: "",
      });
      const outlookFilterKeep = fromMessageFilter({
        action: { delete: false },
        conditions: {},
        displayTitle: "",
      });
      expect(outlookFilterDelete.actions.delete).toBeTruthy();
      expect(outlookFilterKeep.actions.delete).toBeFalsy();
    });

    it("Adds the forwardTo action if present", () => {
      const outlookFilterForwardTo = fromMessageFilter({
        action: { forwardTo: ["test@example.com", "james@example.com"] },
        conditions: {},
        displayTitle: "",
      });
      expect(outlookFilterForwardTo.actions.forwardTo).toEqual([
        { emailAddress: { address: "test@example.com" } },
        { emailAddress: { address: "james@example.com" } },
      ]);
    });

    it("Adds the markAsRead action if present", () => {
      const outlookFilterMarkAsRead = fromMessageFilter({
        action: { markAsRead: true },
        conditions: {},
        displayTitle: "",
      });
      const outlookFilterDontMarkAsRead = fromMessageFilter({
        action: { markAsRead: false },
        conditions: {},
        displayTitle: "",
      });
      expect(outlookFilterMarkAsRead.actions.markAsRead).toBeTruthy();
      expect(outlookFilterDontMarkAsRead.actions.markAsRead).toBeFalsy();
    });

    it("Adds the moveToFolder action if present", () => {
      const outlookFilterMoveToFolder = fromMessageFilter({
        action: { moveToFolder: "ignore" },
        conditions: {},
        displayTitle: "",
      });
      expect(outlookFilterMoveToFolder.actions.moveToFolder).toEqual("ignore");
    });

    it("Ignores the removeLabels action (removeLabels it does not apply to Outlook)", () => {
      const outlookFilter = fromMessageFilter({
        action: { removeLabels: ["personal", "work"] },
        conditions: {},
        displayTitle: "",
      });
      expect(outlookFilter.actions).toEqual({});
    });
  });

  describe("Given some app message filter conditions", () => {
    it("Adds any adddress specified in 'cc' or 'ccOrTo' to the 'recipientContains' list", () => {
      const outlookFilterCC = fromMessageFilter({
        action: {},
        conditions: { cc: ["partners@example.com"] },
        displayTitle: "",
      });
      expect(outlookFilterCC.conditions.recipientContains).toContain("partners@example.com");

      const outlookFilterCcOrTo = fromMessageFilter({
        action: {},
        conditions: { ccOrTo: ["admin@example.com"] },
        displayTitle: "",
      });
      expect(outlookFilterCcOrTo.conditions.recipientContains).toContain("admin@example.com");
    });

    it("Adds any adddress specified in 'to' to the 'sentToAddresses' field", () => {
      const outlookFilterTo = fromMessageFilter({
        action: {},
        conditions: { to: ["admin@example.com"] },
        displayTitle: "",
      });
      expect(outlookFilterTo.conditions.sentToAddresses).toEqual([
        {
          emailAddress: { address: "admin@example.com" },
        },
      ]);
    });

    it("Adds any address specified in 'from' to the 'fromAddresses' field", () => {
      const outlookFilterFrom = fromMessageFilter({
        action: {},
        conditions: { from: ["admin@example.com", "partners@example.com"] },
        displayTitle: "",
      });
      expect(outlookFilterFrom.conditions.fromAddresses).toContain({
        emailAddress: { address: "admin@example.com" },
      });
      expect(outlookFilterFrom.conditions.fromAddresses).toContain({
        emailAddress: { address: "partners@example.com" },
      });
    });

    it("Sets the hasAttachments field if the hasAttachment field is set on the app message rule", () => {
      const outlookFilterHasAttachments = fromMessageFilter({
        action: {},
        conditions: { hasAttachment: true },
        displayTitle: "",
      });
      expect(outlookFilterHasAttachments.conditions.hasAttachments).toBeTruthy();

      const outlookFilterHasNoAttachments = fromMessageFilter({
        action: {},
        conditions: { hasAttachment: false },
        displayTitle: "",
      });
      expect(outlookFilterHasNoAttachments.conditions.hasAttachments).toBeFalsy();
    });

    it("Adds any phrase specified in 'subject' to the 'subjectContains' field", () => {
      const outlookFilterSubject = fromMessageFilter({
        action: {},
        conditions: { subject: ["Lunch Tomorrow", "Party"] },
        displayTitle: "",
      });
      expect(outlookFilterSubject.conditions.subjectContains).toContain("Lunch Tomorrow");
      expect(outlookFilterSubject.conditions.subjectContains).toContain("Party");
    });

    it("Adds a size range predicate if 'size' is specified in the app message rule", () => {
      const outlookFilterSizeLarger = fromMessageFilter({
        action: {},
        conditions: { size: { larger: 1024 } },
        displayTitle: "",
      });
      expect(outlookFilterSizeLarger.conditions.withinSizeRange).toEqual({
        minimumSize: 1024,
        maximumSize: 2 ** 16 - 1,
      });

      const outlookFilterSizeSmaller = fromMessageFilter({
        action: {},
        conditions: { size: { smaller: 4096 } },
        displayTitle: "",
      });
      expect(outlookFilterSizeSmaller.conditions.withinSizeRange).toEqual({
        minimumSize: 0,
        maximumSize: 4096,
      });

      const outlookFilterSizeLargerSmaller = fromMessageFilter({
        action: {},
        conditions: { size: { larger: 1024, smaller: 4096 } },
        displayTitle: "",
      });
      expect(outlookFilterSizeLargerSmaller.conditions.withinSizeRange).toEqual({
        minimumSize: 1024,
        maximumSize: 4096,
      });

      const outlookFilterSizeRange = fromMessageFilter({
        action: {},
        conditions: { size: { range: [1024, 4096] } },
        displayTitle: "",
      });
      expect(outlookFilterSizeRange.conditions.withinSizeRange).toEqual({
        minimumSize: 1024,
        maximumSize: 4096,
      });
    });

    it("Adds a predicate for any phrase the message may contain", () => {
      const outlookFilterPhraseBody = fromMessageFilter({
        action: {},
        conditions: { contains: { body: ["text"] } },
        displayTitle: "",
      });
      expect(outlookFilterPhraseBody.conditions.bodyContains).toContain("text");

      const outlookFilterPhraseBodyOrSubject = fromMessageFilter({
        action: {},
        conditions: { contains: { bodyOrSubject: ["text"] } },
        displayTitle: "",
      });
      expect(outlookFilterPhraseBodyOrSubject.conditions.bodyOrSubjectContains).toContain("text");

      const outlookFilterSubject = fromMessageFilter({
        action: {},
        conditions: { contains: { subject: ["text"] } },
        displayTitle: "",
      });
      expect(outlookFilterSubject.conditions.subjectContains).toContain("text");

      const outlookFilterTo = fromMessageFilter({
        action: {},
        conditions: { contains: { to: ["text"] } },
        displayTitle: "",
      });
      expect(outlookFilterTo.conditions.recipientContains).toContain("text");
    });
  });

  describe("Given some app message exceptions", () => {
    it("Excludes messages matching the exception predicate", () => {
      const outlookFilterException = fromMessageFilter({
        action: {},
        conditions: {},
        displayTitle: "",
        exceptions: {
          cc: ["marketing"],
        },
      });
      expect(outlookFilterException.exceptions.recipientContains).toContain("marketing");
    });
  });
});
