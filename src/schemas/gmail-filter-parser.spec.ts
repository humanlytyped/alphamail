import { fromMessageFilter } from "./gmail-filters-parser";

describe("Creating Gmail filters actions from app message filters action", () => {
  it("Adds the 'TRASH' label to the email, if the 'delete' action is 'true'", () => {
    const gmailFilter = fromMessageFilter({
      action: { delete: true },
      conditions: {},
      displayTitle: "",
    });
    expect(gmailFilter.action.addLabelIds.includes("TRASH")).toBeTruthy();
  });

  it("Adds the labels specified, if the 'addLabels' action was specified", () => {
    const gmailFilter = fromMessageFilter({
      action: { addLabels: ["Dev", "Opensource"] },
      conditions: {},
      displayTitle: "",
    });
    expect(gmailFilter.action.addLabelIds).toEqual(["Dev", "Opensource"]);
  });

  it("Adds the folder path as a label to the message, if the 'copyToFolder' action was specified", () => {
    const gmailFilter = fromMessageFilter({
      action: { copyToFolder: "birdwatchers/europe" },
      conditions: {},
      displayTitle: "",
    });
    expect(gmailFilter.action.addLabelIds.includes("birdwatchers/europe")).toBeTruthy();
  });

  it("Adds the forwarding address if it is specified in the 'forwardTo' action", () => {
    const gmailFilter = fromMessageFilter({
      action: { forwardTo: ["james@example.com", "harry@example.com"] },
      conditions: {},
      displayTitle: "",
    });
    expect(gmailFilter.action.forward).toEqual("james@example.com");
  });

  it("Removes the label ID 'UNREAD' if the 'markAsRead' action is 'true'", () => {
    const gmailFilter = fromMessageFilter({
      action: { markAsRead: true },
      conditions: {},
      displayTitle: "",
    });
    expect(gmailFilter.action.removeLabelIds).toContain("UNREAD");
  });

  it("Labels the message with the folder name as label ID and removes the 'INBOX' label, if the 'moveToFolder' action is specified", () => {
    const gmailFilter = fromMessageFilter({
      action: { moveToFolder: "Birds" },
      conditions: {},
      displayTitle: "",
    });
    expect(gmailFilter.action.addLabelIds).toContain("Birds");
    expect(gmailFilter.action.removeLabelIds).toContain("INBOX");
  });

  it("Removes the specified labels if the 'removeLabels' action is specified", () => {
    const gmailFilter = fromMessageFilter({
      action: { removeLabels: ["Albatross", "Seagull"] },
      conditions: {},
      displayTitle: "",
    });
    expect(gmailFilter.action.removeLabelIds).toContain("Albatross");
    expect(gmailFilter.action.removeLabelIds).toContain("Seagull");
  });
});

describe("Creating Gmail filter criteria from app message filter rule", () => {
  it("Sets the 'from' address to that specified, if a single 'From' address is specified", () => {
    const gmailFilterSingleFrom = fromMessageFilter({
      action: {},
      conditions: { from: ["james@example.com"] },
      displayTitle: "",
    });
    expect(gmailFilterSingleFrom.criteria.from).toEqual("james@example.com");
  });

  it("Adds an OR query for the 'From' addresses, if more than one was specified", () => {
    const gmailFilterMultipleFrom = fromMessageFilter({
      action: {},
      conditions: { from: ["james@example.com", "helen@example.com"] },
      displayTitle: "",
    });
    expect(gmailFilterMultipleFrom.criteria.query).toContain("{from:james@example.com from:helen@example.com}");
  });

  it("Adds an OR query for the addresses in the 'Cc' header, if any was specified", () => {
    const gmailFilterCc = fromMessageFilter({
      action: {},
      conditions: { cc: ["james@example.com", "helen@example.com"] },
      displayTitle: "",
    });
    expect(gmailFilterCc.criteria.query).toContain("{cc:james@example.com cc:helen@example.com}");
  });

  it("Adds an OR query for both the 'Cc' addresses and 'To' addresses, if the ccOrTo addresses was specified", () => {
    const gmailFilterCcOrTo = fromMessageFilter({
      action: {},
      conditions: { ccOrTo: ["james@example.com", "greg@example.com"] },
      displayTitle: "",
    });
    expect(gmailFilterCcOrTo.criteria.query).toContain(
      "{cc:james@example.com cc:greg@example.com to:james@example.com to:greg@example.com}",
    );
  });

  it("Sets the 'hasAttachment' criterion correctly, if it is set in the filter rule", () => {
    const gmailFilterHasAttachment = fromMessageFilter({
      action: {},
      conditions: { hasAttachment: true },
      displayTitle: "",
    });
    const gmailFilterNoAttachment = fromMessageFilter({
      action: {},
      conditions: { hasAttachment: false },
      displayTitle: "",
    });
    expect(gmailFilterHasAttachment.criteria.hasAttachment).toBeTruthy();
    expect(gmailFilterNoAttachment.criteria.hasAttachment).toBeFalsy();
  });

  describe("Setting the size comparison for the filter criteria", () => {
    const gmailFilterSizeRange = fromMessageFilter({
      action: {},
      conditions: { size: { range: [1024, 8196] } },
      displayTitle: "",
    });
    it("Sets the lower bound of the size range to 1 less than that specified in the filter rule", () => {
      expect(gmailFilterSizeRange.criteria.query).toContain(`larger:${1024 * 1024 - 1}`);
    });

    it("Sets the lower bound of the size range to 1 greater than that specified in the filter rule", () => {
      expect(gmailFilterSizeRange.criteria.query).toContain(`smaller:${8196 * 1024 + 1}`);
    });

    const gmailFilterSizeSmaller = fromMessageFilter({
      action: {},
      conditions: { size: { smaller: 8196 } },
      displayTitle: "",
    });
    const gmailFilterSizeLarger = fromMessageFilter({
      action: {},
      conditions: { size: { larger: 1024 } },
      displayTitle: "",
    });
    it("Sets the size comparison exactly as specified, if only 'smaller' or 'larger' is specified", () => {
      expect(gmailFilterSizeSmaller.criteria.size).toEqual(8196 * 1024);
      expect(gmailFilterSizeSmaller.criteria.sizeComparison).toEqual("smaller");

      expect(gmailFilterSizeLarger.criteria.size).toEqual(1024 * 1024);
      expect(gmailFilterSizeLarger.criteria.sizeComparison).toEqual("larger");
    });

    describe("With more than one field set", () => {
      const gmailFilterRangeAndOtherFields = fromMessageFilter({
        action: {},
        conditions: {
          size: { range: [1024, 8196], smaller: 4096, larger: 2048 },
        },
        displayTitle: "",
      });

      it("Gives highest priority to the 'range' field", () => {
        expect(gmailFilterRangeAndOtherFields.criteria.size).toBeUndefined();
        expect(gmailFilterRangeAndOtherFields.criteria.sizeComparison).toBeUndefined();
        expect(gmailFilterRangeAndOtherFields.criteria.query).toContain(`larger:${1024 * 1024 - 1}`);
        expect(gmailFilterRangeAndOtherFields.criteria.query).toContain(`smaller:${8196 * 1024 + 1}`);
      });

      it("Uses 'range' semantics if only 'smaller' and 'larger' are set", () => {
        const gmailFilterSmallerAndLarger = fromMessageFilter({
          action: {},
          conditions: {
            size: {
              larger: 1024,
              smaller: 8196,
            },
          },
          displayTitle: "",
        });
        expect(gmailFilterSmallerAndLarger.criteria.query).toContain(`larger:${1024 * 1024 - 1}`);
        expect(gmailFilterSmallerAndLarger.criteria.query).toContain(`smaller:${8196 * 1024 + 1}`);
      });
    });
  });

  describe("Given only one 'TO' address", () => {
    it("Sets the 'to' field of the filter criteria to the given address", () => {
      const filter = fromMessageFilter({
        action: {},
        conditions: { to: ["greg@example.com"] },
        displayTitle: "",
      });
      expect(filter.criteria.to).toEqual("greg@example.com");
    });
  });

  describe("Given more than one 'TO' addresses", () => {
    it("Adds an OR query for each of the addresses in the TO header", () => {
      const emails = ["greg@example.com", "bernard@example.com", "bob@example.com"];
      const filter = fromMessageFilter({
        action: {},
        conditions: {
          to: emails,
        },
        displayTitle: "",
      });
      emails.map(e => "to:" + e).forEach(e => expect(filter.criteria.query).toContain(e));
    });
  });

  describe("Given only one message subject", () => {
    it("Sets the 'subject' field of the filter criteria to the given subject", () => {
      const filter = fromMessageFilter({
        action: {},
        conditions: { subject: ["Bird Watching Retreat"] },
        displayTitle: "",
      });
      expect(filter.criteria.subject).toEqual("Bird Watching Retreat");
    });
  });

  describe("Given more than one message subjects", () => {
    it("Adds a query for an exact match on any of the subjects", () => {
      const subjects = ["Bird Watching Retreat", "Maintenance Notice for Your Application", "Upcoming Chess Game"];
      const filter = fromMessageFilter({
        action: {},
        conditions: {
          subject: subjects,
        },
        displayTitle: "",
      });
      subjects.map(e => `subject:"${e}"`).forEach(e => expect(filter.criteria.query).toContain(e));
    });
  });

  describe("Given exceptions for messages", () => {
    it("Adds a negated query matching the exceptions specified", () => {
      const gmailFilter = fromMessageFilter({
        action: {},
        conditions: {},
        displayTitle: "",
        exceptions: {
          from: ["engineering@sprinthubmobile.com", "bob@example.com"],
          hasAttachment: false,
          size: { range: [1024, 8196] },
          subject: ["pizza", "meeting"],
        },
      });
      expect(gmailFilter.criteria.negatedQuery).toEqual(
        `{from:engineering@sprinthubmobile.com from:bob@example.com} -has:attachment larger:${1024 * 1024 -
          1} smaller:${8196 * 1024 + 1} {subject:"pizza" subject:"meeting"}`,
      );

      const gmailFilterLargeOrAttachments = fromMessageFilter({
        action: {},
        conditions: {},
        displayTitle: "",
        exceptions: {
          hasAttachment: true,
          size: {
            larger: 8196,
            smaller: 20480,
          },
        },
      });
      expect(gmailFilterLargeOrAttachments.criteria.negatedQuery).toEqual(
        `has:attachment larger:${8196 * 1024 - 1} smaller:${20480 * 1024 + 1}`,
      );

      const gmailLarger = fromMessageFilter({
        action: {},
        conditions: {},
        displayTitle: "",
        exceptions: { size: { larger: 4196 } },
      });
      expect(gmailLarger.criteria.negatedQuery).toEqual(`larger:${4196 * 1024}`);

      const gmailSmaller = fromMessageFilter({
        action: {},
        conditions: {},
        displayTitle: "",
        exceptions: { size: { smaller: 2048 } },
      });
      expect(gmailSmaller.criteria.negatedQuery).toEqual(`smaller:${2048 * 1024}`);

      const gmailFilterTo = fromMessageFilter({
        action: {},
        conditions: {},
        displayTitle: "",
        exceptions: { to: ["gary", "bob"] },
      });
      expect(gmailFilterTo.criteria.negatedQuery).toEqual('{to:"gary" to:"bob"}');

      const filterCcTo = fromMessageFilter({
        action: {},
        conditions: {},
        displayTitle: "",
        exceptions: { cc: ["gary", "Greg"], ccOrTo: ["James"] },
      });
      expect(filterCcTo.criteria.negatedQuery).toContain('{cc:"gary" cc:"Greg"}');
      expect(filterCcTo.criteria.negatedQuery).toContain('{cc:"James" to:"James"}');

      const filterContains = fromMessageFilter({
        action: {},
        conditions: {},
        displayTitle: "",
        exceptions: { contains: { body: ["Call of Duty", "Doom Eternal"] } },
      });
      expect(filterContains.criteria.negatedQuery).toContain('{"Call of Duty" "Doom Eternal"}');
    });
  });
  describe("Given some text to match in some part of the message", () => {
    it("Adds an OR query for each of the text in the corresponding part of the message", () => {
      const gmailFilterBodyContent = fromMessageFilter({
        action: {},
        conditions: {
          contains: { body: ["pizza", "new learning resource", "new project"] },
        },
        displayTitle: "",
      });
      expect(gmailFilterBodyContent.criteria.query).toContain('{"pizza" "new learning resource" "new project"}');

      const gmailFilterBodyOrSubject = fromMessageFilter({
        action: {},
        conditions: {
          contains: {
            bodyOrSubject: ["pizza", "new learning resource", "new project"],
          },
        },
        displayTitle: "",
      });
      expect(gmailFilterBodyOrSubject.criteria.query).toContain(
        '{"pizza" "new learning resource" "new project"} OR subject:{"pizza" "new learning resource" "new project"}',
      );

      const gmailFilterSubjectContent = fromMessageFilter({
        action: {},
        conditions: {
          contains: { subject: ["pizza", "new project"] },
        },
        displayTitle: "",
      });
      expect(gmailFilterSubjectContent.criteria.query).toContain('subject:{"pizza" "new project"}');

      const gmailFilterTo = fromMessageFilter({
        action: {},
        conditions: {
          contains: { to: ["greg", "james"] },
        },
        displayTitle: "",
      });
      expect(gmailFilterTo.criteria.query).toContain('to:{"greg" "james"}');
    });
  });
});
