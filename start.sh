#!/bin/env bash

WWW_ROOT_DIR=$(pwd)/dist/alphamail $(pwd)/caddy_server/bin/caddy -agree -http-port $PORT
